
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <dht11.h>


#define PowerPin1 7
#define DHT11PIN A2 

// use serial print to debug; comment next line to disable serial.
// #define DEBUG

// use low power sleep; comment next line to not use low power sleep.
#define SLEEP

#ifdef SLEEP
#include "LowPower.h"
bool next = false;
#endif

// Pin defination and coefficients for the Sharp dust sensor.
const int sharpLEDPin = A1;
const int sharpVoPin = A0;
static float Voc = 0.6;
const float K = 0.4;
unsigned int samplingTime = 280;
unsigned int deltaTime = 40;
unsigned int sleepTime = 9680;

// Initialize a Dht11 object.
dht11 DHT11;




// Node Configuration, Update Network session key, Application session key and the Device Address of your device. 
static const PROGMEM u1_t NWKSKEY[16] = { 0x00, 0xBD, 0x42, 0x5B, 0x9C, 0x18, 0xC6, 0x62, 0x94, 0xDD, 0x6A, 0xA6, 0xEC, 0x2F, 0x11, 0x00 };
static const u1_t PROGMEM APPSKEY[16] = { 0x00, 0xF5, 0xBC, 0x0C, 0xA9, 0xB2, 0xDF, 0x72, 0x09, 0x9B, 0x0A, 0x27, 0xA0, 0xED, 0x00, 0x00 };
static const u4_t DEVADDR = 0x26000000; // <-- Change this address for every node!


// Schedule TX every this many seconds (might become longer due to duty cycle limitations).
const unsigned TX_INTERVAL =3600;

// Parameters/Packet to be sent.
struct {
  short int battVoltage;
  short int dustDensity;
  short int atmHumidity;
  short int atmTemperature;
} mydata;


void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

static osjob_t sendjob;

// Pin mapping.
const lmic_pinmap lmic_pins = {
  .nss = 6,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 5,
  .dio = {2, 3, 4},
};


void onEvent (ev_t ev) {
#ifdef DEBUG
  Serial.println(F("Enter onEvent"));
#endif
  switch (ev) {
    case EV_SCAN_TIMEOUT:
    #ifdef DEBUG
      Serial.println(F("EV_SCAN_TIMEOUT"));
      #endif
      break;
    case EV_BEACON_FOUND:
    #ifdef DEBUG
      Serial.println(F("EV_BEACON_FOUND"));
      #endif
      break;
    case EV_BEACON_MISSED:
    #ifdef DEBUG
      Serial.println(F("EV_BEACON_MISSED"));
      #endif
      break;
    case EV_BEACON_TRACKED:
    #ifdef DEBUG
      Serial.println(F("EV_BEACON_TRACKED"));
      #endif
      break;
    case EV_JOINING:
    #ifdef DEBUG
      Serial.println(F("EV_JOINING"));
      #endif
      break;
    case EV_JOINED:
    #ifdef DEBUG
      Serial.println(F("EV_JOINED"));
      #endif
      break;
    case EV_RFU1:
    #ifdef DEBUG
      Serial.println(F("EV_RFU1"));
      #endif
      break;
    case EV_JOIN_FAILED:
    #ifdef DEBUG
      Serial.println(F("EV_JOIN_FAILED"));
      #endif
      break;
    case EV_REJOIN_FAILED:
    #ifdef DEBUG
      Serial.println(F("EV_REJOIN_FAILED"));
      #endif
      break;
    case EV_TXCOMPLETE:
    #ifdef DEBUG   
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
      #endif
    if (LMIC.dataLen) {
      // data received in rx slot after tx
      #ifdef DEBUG
      Serial.print(F("Data Received: "));
      Serial.write(LMIC.frame + LMIC.dataBeg, LMIC.dataLen);
      Serial.println();
      #endif
    }
    #ifndef SLEEP
    os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
    #else
    next = true;
    #endif
    break;
    case EV_LOST_TSYNC:
    #ifdef DEBUG
      Serial.println(F("EV_LOST_TSYNC"));
      #endif
      break;
    case EV_RESET:
    #ifdef DEBUG
      Serial.println(F("EV_RESET"));
      #endif
      break;
    case EV_RXCOMPLETE:
    #ifdef DEBUG
      // data received in ping slot
      Serial.println(F("EV_RXCOMPLETE"));
      #endif
      break;
    case EV_LINK_DEAD:
    #ifdef DEBUG
      Serial.println(F("EV_LINK_DEAD"));
      #endif
      break;
    case EV_LINK_ALIVE:
    #ifdef DEBUG
      Serial.println(F("EV_LINK_ALIVE"));
      #endif
      break;
    default:
    #ifdef DEBUG
      Serial.println(F("Unknown event"));
      #endif
      break;
}
#ifdef DEBUG
  Serial.println(F("Leave onEvent"));
#endif
}

void do_send(osjob_t* j) {
  readTemHum_DHT11();
  mydata.battVoltage = readBatteryVoltage();
  mydata.dustDensity = readDustConcentration();

  #ifdef DEBUG
    Serial.println(F("Enter do_send"));
  #endif

  if (LMIC.opmode & OP_TXRXPEND) {
    #ifdef DEBUG
    Serial.println(F("OP_TXRXPEND, not sending"));
    #endif
  } else { 
    // Prepare upstream data transmission at the next possible time.
    LMIC_setTxData2(1, (unsigned char *)&mydata, sizeof(mydata) - 1, 0);
    #ifdef DEBUG
    Serial.println(F("Packet queued"));
    #endif
  }  
  #ifdef DEBUG
    Serial.println(F("Leave do_send"));
  #endif
}




void setup(){
  
  #ifdef DEBUG
  Serial.begin(9600);
  #endif
  
  // Setting pin. 
  pinMode(sharpLEDPin, OUTPUT);
  pinMode(sharpVoPin,INPUT);
  pinMode(PowerPin1, OUTPUT);
  
 
// LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();

  // Set static session parameters. Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
#ifdef PROGMEM
  // On AVR, these values are stored in flash and only copied to RAM
  // once. Copy them to a temporary buffer here, LMIC_setSession will
  // copy them into a buffer of its own again.
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
#else
  // If not running an AVR with PROGMEM, just use the arrays directly
  LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
#endif

#if defined(CFG_eu868) 
  LMIC_setupChannel(0, 865062500, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(1, 865402500, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
  LMIC_setupChannel(2, 865985000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band 
#elif defined(CFG_us915) 
  LMIC_selectSubBand(1);
#endif

  // Disable link check validation
  LMIC_setLinkCheckMode(0);

  // TTN uses SF9 for its RX2 window.
  LMIC.dn2Dr = DR_SF9;

  // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7, 14);

  // Start job
  do_send(&sendjob);

  // Wait a maximum of 10s for Serial Monitor
}


void loop() {

  #ifndef SLEEP
    os_runloop_once();
  #else
    extern volatile unsigned long timer0_overflow_count;
    if (next == false) {
      os_runloop_once();
    } else {
      int sleepcycles = TX_INTERVAL / 8;  // calculate the number of sleepcycles (8s) given the TX_INTERVAL

  #ifdef DEBUG
  Serial.print(F("Enter sleeping for "));
  Serial.print(sleepcycles);
  Serial.println(F(" cycles of 8 seconds"));
  #endif
  
  Serial.flush(); // give the serial print chance to complete
  for (int i = 0; i < sleepcycles; i++) {
    // Enter power down state for 8 s with ADC and BOD module disabled
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    // LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, SPI_OFF, USART0_OFF, TWI_OFF);
    // LMIC uses micros() to keep track of the duty cycle, so
    // hack timer0_overflow for a rude adjustment:
    cli();
    timer0_overflow_count += 8 * 64 * clockCyclesPerMicrosecond();
    sei();
  }

  #ifdef DEBUG
  Serial.println(F("Sleep complete"));
  #endif
      next = false;

      // Start job
      do_send(&sendjob);
    }
#endif

}
//DHT temperature and humidity sensor

void readTemHum_DHT11(){
  int chk = DHT11.read(DHT11PIN);

  mydata.atmHumidity = DHT11.humidity;
  mydata.atmTemperature = DHT11.temperature;

  #ifdef DEBUG
  Serial.print("Humidity (%): ");
  Serial.println((float)DHT11.humidity, 2);
  Serial.print("Temperature (C): ");
  Serial.println((float)DHT11.temperature, 2);
  #endif  
}

//Dust sensor

short int readDustConcentration(){
  int  VoRaw = 0;
  float Vo = 0, dustDensitysum = 0, dustDensity = 0;

  // switch ON the Power for dust Sensor.
  digitalWrite(PowerPin1, HIGH);
  delay(20);

// read the value from the sensor for 100 samples.
  for(int i=0;i<100;i++){
    digitalWrite(sharpLEDPin, LOW);
    delayMicroseconds(samplingTime);
    VoRaw = analogRead(sharpVoPin);
    delayMicroseconds(deltaTime);
    digitalWrite(sharpLEDPin,HIGH);
    delayMicroseconds(sleepTime); 
    Vo = VoRaw;  
    Vo = Vo / 1024.0 * 3.3;    
    float dV = Vo - Voc;
    if ( dV < 0 ) {
      dV = 0;
      Voc = Vo;
    }
    
    dustDensity = dV / K * 100.0;
    dustDensitysum = dustDensitysum+dustDensity;
  }
  
  #ifdef DEBUG   
  Serial.print("voltage:");
  Serial.print(Vo*1000);
  Serial.println("mV");
  #endif
  
  dustDensity = dustDensitysum/100;
  digitalWrite(PowerPin1,LOW);
  delay(20);

  #ifdef DEBUG 
  Serial.print("Dustdensity=");
  Serial.print(dustDensity);
  Serial.println("ug/m3");
  #endif

  return dustDensity;
}


//Battery Monitor

int readBatteryVoltage() {

  int batt_millivolt = 0;
  float analogvalue=0,battVolt=0;
  for (byte  i = 0; i < 10; i++){
      analogvalue += analogRead(A3);
      delay(5);     
  }
  analogvalue=analogvalue/10;
  #ifdef DEBUG
  Serial.print("analogvalue= ");               
  Serial.println(analogRead(A3));
  #endif

  battVolt = ((analogvalue* 3.3)/1024)*2;  //ADC voltage*Ref. Voltage/1024    
  batt_millivolt = battVolt*100;

  #ifdef DEBUG
  Serial.print("Voltage= ");               
  Serial.print(battVolt);
  Serial.println("V");
  #endif

  return batt_millivolt;
  }
